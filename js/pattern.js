//泡饮料(抽象的父类)

// 创建成构造函数
var Beverage = function() {};

//方法写在构造函数的原型上
// prototype 属性 : 使您有能力向对象添加属性和方法
Beverage.prototype.boilWater = function () {
    console.log('把水煮沸');
};
Beverage.prototype.brew = function () {
    throw new Error('子类必须重写该方法');
};
Beverage.prototype.pourInCup = function () {
    throw new Error('子类必须重写该方法');
};
Beverage.prototype.addCondiments = function () {
    throw new Error('子类必须重写该方法');
};
//钩子方法
Beverage.prototype.customerWantsCondiments = function() {
    return true;
};

Beverage.prototype.init = function() {
    this.boliWater();
    this.brew();
    this.pourInCup();
    if(this.customerWantsCondiments()) {    //判断是否要加调料
        this.addCondiments();
    };

};


//泡一杯咖啡
var Coffee = function() {};

Coffee.prototype.brew = function () {
    console.log('用沸水冲泡咖啡');
};
Coffee.prototype.pourInCup = function () {
    console.log('把咖啡倒进杯子');
};
Coffee.prototype.addCondiments = function () {
    console.log('加糖和牛奶');
};


// 泡一杯茶
var Tea = function() {};

Tea.prototype.brew = function () {
    console.log('用沸水浸泡茶叶');
};
Tea.prototype.pourInCup = function () {
    console.log('把茶水倒进杯子');
};
Tea.prototype.addCondiments = function () {
    console.log('加柠檬');
};
Tea.prototype.customerWantsCondiments = function() {
    return window.confirm('请问需要加调料吗？');     //弹出提问对话框
};

//继承
Coffee.prototype = new Beverage();
Tea.prototype = new Beverage();

//创建coffee对象，实例化Coffee()构造函数
var coffee = new Coffee();
//调用init方法
coffee.init();

var tea = new Tea();
tea.init();